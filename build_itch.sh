rm -rf alasa.zip
cargo build --target wasm32-unknown-unknown --release
wasm-bindgen --out-dir ./out/ --target web ./target/wasm32-unknown-unknown/release/alasa.wasm
zip alasa.zip index.html assets/**/* out/*