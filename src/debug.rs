use bevy::prelude::*;
use bevy_inspector_egui::{WorldInspectorPlugin, RegisterInspectable};

use crate::player::Player;
use crate::enemy_attacks::Health;
use crate::slimes::Slime;

pub struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        
        // Checks if we are in a debug build
        // Learn: this is puke to me
        if cfg!(debug_assertions) {
            app.add_plugin(WorldInspectorPlugin::new())
                .register_inspectable::<Player>()
                .register_inspectable::<Health>()
                .register_inspectable::<Slime>()
                ;
        }
    }
}