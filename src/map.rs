use bevy::{prelude::*};

use crate::{RESOLUTION, GameStates};

pub struct MapPlugin;

const TILESIZESCALE: f32 = 0.1;
const TILESIZE: f32 = 32.0; 

const GRASSTILES:   [usize; 16] = [0, 1, 2, 3, 8, 9, 10, 11, 16, 17, 18, 19, 24, 25, 26, 27];
const FLOWERSTILES: [usize; 16] = [4, 5, 6, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29, 30, 31];
const ROADTILES:    [usize; 30] = [
    32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62
];


#[derive(Resource)]
struct TilemapSheet(Handle<TextureAtlas>);

#[derive(Component)]
pub struct Map;

#[derive(Component)]
pub struct Tile;

#[derive(Resource, Debug)]
struct MapGrid {
    x_tiles: f32,
    y_tiles: f32
}


impl Plugin for MapPlugin {
    fn build(&self, app: &mut App) {
        app
        .add_startup_system_to_stage(StartupStage::PreStartup, load_tilemap)
        .add_startup_system(spawn_tiles_to_window_size.label("spawn_tiles"))
        .add_system_set(
            SystemSet::on_enter(GameStates::Combat).with_system(show_tiles)
        ).add_system_set(
            SystemSet::on_exit(GameStates::Combat).with_system(hide_tiles)
        )
        .add_system_set(
            SystemSet::on_update(GameStates::Combat)
                .with_system(tile_spawn_handler)
        )   
        ;
    }
}


fn load_tilemap(mut commands: Commands, assets: Res<AssetServer>, mut texture_atlases: ResMut<Assets<TextureAtlas>>) {
    let image = assets.load("tilemap/tx_tileset_grass.png");
    let atlas = TextureAtlas::from_grid(
        image,
        Vec2::splat(TILESIZE),
        8,
        8,
        None,
        None,
    );

    let atlas_handle = texture_atlases.add(atlas);

    commands.insert_resource(TilemapSheet(atlas_handle));
}

fn spawn_tile(
    commands: &mut ChildBuilder,
    sheet: &TilemapSheet,
    translation: Vec3
) {

    let x = (translation.x.abs() * 10.0) as usize  % 10;
    let y = (translation.y.abs() * 10.0) as usize  % 10;


    // TODO: change for variety
    let index = match (x, y) {
        (3, i) => ROADTILES[i],
        (i, 3) => ROADTILES[i],
        (8, i) => FLOWERSTILES[i],
        (i, 0) => FLOWERSTILES[i],
        (i, j) => GRASSTILES[(i + j) % 16]
    };

    let mut sprite = TextureAtlasSprite::new(index);
    sprite.custom_size = Some(Vec2::splat(TILESIZESCALE));

    commands.spawn((
        SpriteSheetBundle {
            sprite: sprite,
            texture_atlas: sheet.0.clone(),
            transform: Transform {
                translation: translation,
                ..Default::default()
            },
            visibility: Visibility { is_visible: true },
            ..Default::default()
        },
        Name::new("Tile"),
        Tile,
    ));
}

fn spawn_tiles_to_window_size(
        mut commands: Commands, 
        tilemap_sheet: Res<TilemapSheet>
) {

    let tile_padding = 1.0;
    let x_tiles = ((2.0 * RESOLUTION) / TILESIZESCALE) + 2.0 * tile_padding;
    let start_x_tile = -1.0 * RESOLUTION - (TILESIZESCALE * tile_padding);
    let y_tiles = (2.0 / TILESIZESCALE) + 2.0 * tile_padding;
    let start_y_tile = -1.0 - (TILESIZESCALE * tile_padding);

    commands.insert_resource(MapGrid {
        x_tiles,
        y_tiles,
    });


    println!("{}, {}, {}", x_tiles, y_tiles, x_tiles * y_tiles);


    let map = commands.spawn((
        SpatialBundle {
            transform: Transform::default(),
            visibility: Visibility {
                is_visible: false,
            },
            ..Default::default()
        },
        Map,
        Name::new("Map"),
    )).id();

    for x_index in 0..x_tiles as usize {
        for y_index in 0..y_tiles as usize {
            // TODO: smarter pattern
            let index = (x_index + y_index * 8) % 62;

            commands.entity(map).with_children(|commands| {            
                let mut sprite = TextureAtlasSprite::new(index);
                sprite.custom_size = Some(Vec2::splat(TILESIZESCALE));
                
                spawn_tile(
                    commands,
                    &tilemap_sheet,
                    Vec3::new( start_x_tile + (x_index as f32) * TILESIZESCALE, start_y_tile + (y_index as f32) * TILESIZESCALE, 1.0)
                );
            });
        }
    }
}


fn tile_spawn_handler(
    mut commands: Commands,
    tile_query: Query<(Entity, &Transform, With<Tile>)>,
    camera_query: Query<&Transform, With<Camera>>,
    tilemap_sheet: Res<TilemapSheet>, 
    map_grid: Res<MapGrid>,
    map_query: Query<Entity, With<Map>>
) {
    let map = map_query.single();

    let camera_transform = camera_query.single();

    let left_edge = -1.0 * RESOLUTION - TILESIZESCALE + camera_transform.translation.x;
    let right_edge = 1.0 * RESOLUTION + TILESIZESCALE + camera_transform.translation.x;
    let top_edge = -1.0 - TILESIZESCALE + camera_transform.translation.y;
    let bottom_edge = 1.0 + TILESIZESCALE + camera_transform.translation.y;

    // let mut counter = 0;
    // let mut change = false;


    for (entity, tile_transform, _) in tile_query.iter() {
        // counter += 1;

        let translation = tile_transform.translation;
        
        if translation.x < left_edge {
            // change = true;
            commands.entity(entity).despawn_recursive();

            let new_translation_edge = translation.x + (map_grid.x_tiles * TILESIZESCALE);
            if !(new_translation_edge > right_edge) {
                commands.entity(map).with_children(|child_commands| {
                    spawn_tile(
                        child_commands,
                        &tilemap_sheet,
                        Vec3::new(new_translation_edge, translation.y, 1.0),
                    )
                });
            }
        } else if translation.x > right_edge {
            // change = true;
            commands.entity(entity).despawn_recursive();

            let new_translation_edge = translation.x - (map_grid.x_tiles * TILESIZESCALE);
            if !(new_translation_edge < left_edge) {
                commands.entity(map).with_children(|child_commands| {
                    spawn_tile(
                        child_commands,
                        &tilemap_sheet,
                        Vec3::new(new_translation_edge, translation.y, 1.0),
                    )
                });
            }
        } else if translation.y < top_edge {
            // change = true;
            commands.entity(entity).despawn_recursive();

            let new_translation_edge = translation.y + (map_grid.y_tiles * TILESIZESCALE);
            if new_translation_edge < bottom_edge {
                commands.entity(map).with_children(|child_commands| {
                    spawn_tile(
                        child_commands,
                        &tilemap_sheet,
                        Vec3::new(translation.x, new_translation_edge, 1.0),
                    )
                });
            }
        } else if translation.y > bottom_edge {
            // change = true;
            commands.entity(entity).despawn_recursive();

            let new_translation_edge = translation.y - (map_grid.y_tiles * TILESIZESCALE);
            if new_translation_edge > top_edge {
                commands.entity(map).with_children(|child_commands| {
                    spawn_tile(
                        child_commands,
                        &tilemap_sheet,
                        Vec3::new(translation.x, new_translation_edge, 1.0),
                    )
                });
            }
        }
    }

    // // For debugging if orphan tiles
    // if change {
    //     println!("number of tiles: {}", counter);
    // }

}


fn hide_tiles(
    mut map_query: Query<&mut Visibility, With<Map>>,
) {
    let mut map = map_query.single_mut();
    map.is_visible = false;
}

fn show_tiles(
    mut map_query: Query<&mut Visibility, With<Map>>,
) {
    let mut map = map_query.single_mut();
    map.is_visible = true;
}
