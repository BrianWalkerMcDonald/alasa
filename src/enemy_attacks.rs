use bevy::{prelude::*, sprite::collide_aabb::collide};
use bevy_inspector_egui::Inspectable;

use crate::GameStates;
use crate::player::{Player, PlayerDeath, PlayerHurt};
use crate::slimes::Slime;

pub struct CombatPlugin;


#[derive(Component, Inspectable, Debug)] 
pub struct Health {
    pub health: u32,
    pub max_health: u32,
}

#[derive(Component)] 
pub struct Attack {
    pub damage: u32,
    pub area: Vec2,
    pub timer: Timer,
}


impl Plugin for CombatPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_update(GameStates::Combat)
                .with_system(handle_attacks)
        );
    }
}


pub fn add_attack_area(
    mut commands: Commands,
    parent_entity: Entity,
    damage: u32,
    area: Vec2,
    offset: Vec2,
    seconds: f32,
) {
    let attack= commands.spawn((
        // // Hit box for Debugging
        // SpriteBundle {
        //     sprite: Sprite {
        //         color: Color::rgb(0.2, 0.2, 0.5),
        //         custom_size: Some(area.clone()),
        //         ..Default::default()
        //     },
        //     ..Default::default()
        // }
        TransformBundle {
            local: Transform::from_translation(Vec3::new(offset.x, offset.y, 0.0)),
            ..Default::default()
        },
        Attack {
            damage,
            area,
            timer: Timer::from_seconds(seconds, TimerMode::Repeating),
        },
        Name::new("Attack Entity")
    )).id();

    commands.entity(parent_entity).push_children(&[attack]);
}


fn handle_attacks(
    mut attacks: Query<(&Parent, &mut Attack, &GlobalTransform)>,
    mut players: Query<(&mut Health, &GlobalTransform, &Player), Without<Slime>>,
    slimes: Query<&Slime, Without<Player>>,
    time: Res<Time>,
    mut death_events: EventWriter<PlayerDeath>,
    mut player_damage_events: EventWriter<PlayerHurt>,
){

    for (parent, mut attack, attack_transform) in attacks.iter_mut() {
        attack.timer.tick(time.delta());

        let slime_attacker = slimes.get(parent.get()).is_ok();

        if slime_attacker && attack.timer.just_finished() {
            for (mut health, target_transform, player) in players.iter_mut() {
            
                let collided = collide(
                    attack_transform.translation(),
                    attack.area,
                    target_transform.translation(),
                    player.collision_size,
                ).is_some();
        
                if collided {
                    health.health = health.health.saturating_sub(attack.damage);
        
                    if health.health == 0 {
                        death_events.send_default()
                    } else {
                        player_damage_events.send_default()
                    }
                }
            }
        }
    }

}
