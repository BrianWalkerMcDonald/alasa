use bevy::prelude::*;
use bevy_kira_audio::prelude::*;

use crate::GameStates;
use crate::player::PlayerAttack;
use crate::player::PlayerHurt;
use crate::slimes::SlimeDeath;

pub struct GameAudioPlugin;

#[derive(Resource)]
pub struct AudioState {
    bgm_handle: Handle<AudioSource>,
    combat_handle: Handle<AudioSource>,
    game_over_handle: Handle<AudioSource>,
    slime_death_handle: Handle<AudioSource>,
    player_hurt_handle: Handle<AudioSource>,
    player_attack_handle: Handle<AudioSource>,

    volume: f64,
    sfx_volume: f64,
}

#[derive(Resource)]
struct MainMenuMusic;

#[derive(Resource)]
struct CombatMusic;

#[derive(Resource)]
struct GameOverMusic;

#[derive(Resource)]
struct SlimeDeathSFX;

#[derive(Resource)]
struct PlayerAttackSFX;

#[derive(Resource)]
struct PlayerHurtSFX;

impl Plugin for GameAudioPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(AudioPlugin)
            .add_audio_channel::<MainMenuMusic>()
            .add_audio_channel::<CombatMusic>()
            .add_audio_channel::<GameOverMusic>()
            .add_audio_channel::<PlayerAttackSFX>()
            .add_audio_channel::<PlayerHurtSFX>()
            .add_audio_channel::<SlimeDeathSFX>()
            .add_startup_system_to_stage(StartupStage::PreStartup, load_audio)
            .add_startup_system(start_bgm_music)
            .add_system_set(
                SystemSet::on_exit(GameStates::MainMenu)
                    .with_system(main_menu_to_combat_music)
            )
            .add_system_set(
                SystemSet::on_exit(GameStates::Combat)
                    .with_system(combat_to_game_over_music)
            )
            .add_system_set(
                SystemSet::on_update(GameStates::Combat)
                    .with_system(player_attack_sfx)
                    .with_system(player_hurt_sfx.before("player_damage_logic"))
                    .with_system(slime_death_sfx.before("slime_death_logic"))
            )
            .add_system_set(
                SystemSet::on_exit(GameStates::GameOver)
                    .with_system(game_over_to_main_menu_music)
            )
        ;

    }
}

fn player_attack_sfx(
    player_attack_channel: Res<AudioChannel<PlayerAttackSFX>>,
    player_attack_event_reader: EventReader<PlayerAttack>,
    audio_states: Res<AudioState>,
) {
    if !player_attack_event_reader.is_empty() {
        player_attack_event_reader.clear();

        player_attack_channel.play(
            audio_states.player_attack_handle.clone()
        ).with_volume(audio_states.sfx_volume);
    }

}

fn player_hurt_sfx(
    player_hurt_channel: Res<AudioChannel<PlayerHurtSFX>>,
    player_hurt_event_reader: EventReader<PlayerHurt>,
    audio_states: Res<AudioState>,
) {
    if !player_hurt_event_reader.is_empty() {
        player_hurt_channel.play(
            audio_states.player_hurt_handle.clone()
        ).with_volume(audio_states.sfx_volume);
    }

}

fn slime_death_sfx(
    slime_death_channel: Res<AudioChannel<SlimeDeathSFX>>,
    slime_death_event_reader: EventReader<SlimeDeath>,
    audio_states: Res<AudioState>,
) {
    if !slime_death_event_reader.is_empty() {
        slime_death_channel.play(
            audio_states.slime_death_handle.clone()
        ).with_volume(audio_states.sfx_volume);
    }

}

fn start_bgm_music(
    background: Res<AudioChannel<MainMenuMusic>>,
    audio_states: Res<AudioState>,
) {
    println!("starting Main Menu music");
    background.play(
        audio_states.bgm_handle.clone()
    ).looped()
    .with_volume(audio_states.volume);
}

fn main_menu_to_combat_music(
    background: Res<AudioChannel<MainMenuMusic>>,
    combat: Res<AudioChannel<CombatMusic>>,
    audio_states: Res<AudioState>,
) {
    println!("pause Main Menu music, start Combat music");
    background.pause();
    combat.play(
        audio_states.combat_handle.clone()
    ).looped()
    .with_volume(audio_states.volume);
}

fn combat_to_game_over_music(
    combat: Res<AudioChannel<CombatMusic>>,
    game_over: Res<AudioChannel<GameOverMusic>>,
    audio_states: Res<AudioState>,
) {
    println!("stop Combat music, start Game Over music");
    combat.stop();
    game_over.play(
        audio_states.game_over_handle.clone()
    ).looped()
    .with_volume(audio_states.volume);
}

fn game_over_to_main_menu_music(
    game_over: Res<AudioChannel<GameOverMusic>>,
    background: Res<AudioChannel<MainMenuMusic>>,
) {
    println!("stop Game Over music, start Main Menu music");
    game_over.stop();
    background.resume();
}   


fn load_audio(
    mut commands: Commands,
    assets: Res<AssetServer>,
) {
    let bgm_handle = assets.load("background_track/RabbitTown.wav");
    let combat_handle = assets.load("background_track/PixelWar2.wav");
    let game_over_handle = assets.load("background_track/SaveTheCity.wav");

    let slime_death_handle = assets.load("jfxr/slime_death.wav");
    let player_hurt_handle = assets.load("jfxr/player_hurt.wav");
    let player_attack_handle = assets.load("jfxr/player_attack.wav");

    let volume = 0.1;
    let sfx_volume = 0.2;

    commands.insert_resource(AudioState {
        bgm_handle,
        combat_handle,
        game_over_handle,
        slime_death_handle,
        player_hurt_handle,
        player_attack_handle,
        volume,
        sfx_volume,
    });    
}