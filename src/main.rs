#![allow(clippy::redundant_field_names)]
use bevy::{prelude::*, window::PresentMode};

mod player;
use player::PlayerPlugin;

mod slimes;
use slimes::SlimePlugin;

mod map;
use map::MapPlugin;

mod title;
use title::TitlePlugin;

mod debug;
use debug::DebugPlugin;

mod camera;
use camera::CameraPlugin;

mod graphics;
use graphics::GraphicsPlugin;

mod enemy_attacks;
use enemy_attacks::CombatPlugin;

mod audio;
use audio::GameAudioPlugin;

pub const WINDOW_HEIGHT: f32 = 770.;
pub const WINDOW_WIDTH: f32 = 1309.;
pub const RESOLUTION: f32 = WINDOW_WIDTH / WINDOW_HEIGHT;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Copy)]
pub enum GameStates {
    MainMenu,
    Combat,
    GameOver,
}


fn main() {
    App::new()
        .add_state(GameStates::MainMenu)
        .add_plugins(
            DefaultPlugins.set(WindowPlugin {
                window: WindowDescriptor {
                    title: "alasa".to_string(),
                    // reflect these window settings in the index.html canvas settings
                    // also set "embed options" on the itch.io for the game
                    width: WINDOW_WIDTH,
                    height: WINDOW_HEIGHT,
                    resizable: false,
                    present_mode: PresentMode::Fifo,
                    ..default()
                },
                ..default()
            })
            .set(ImagePlugin::default_nearest())
        )
        .add_plugin(CameraPlugin)
        .add_plugin(PlayerPlugin)
        .add_plugin(SlimePlugin)
        .add_plugin(GraphicsPlugin)
        .add_plugin(MapPlugin)
        .add_plugin(TitlePlugin)
        .add_plugin(DebugPlugin)
        .add_plugin(CombatPlugin)
        .add_plugin(GameAudioPlugin)
        .run();
}