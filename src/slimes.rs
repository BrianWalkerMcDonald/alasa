use bevy::{prelude::*, sprite::collide_aabb::collide};
use bevy_inspector_egui::Inspectable;
use rand::prelude::*;

use crate::{GameStates, RESOLUTION};
use crate::graphics::FrameAnimation;
use crate::player::Player;
use crate::enemy_attacks::{ Health, add_attack_area };

pub const SLIME_TILE_SCALE: f32 = 0.1;
pub const SLIME_TILE_SIZE: f32 = 16.0;
const SPRITEZ: f32 = 800.0;


pub struct SlimePlugin;


#[derive(Component, Inspectable, Clone, Copy)]
pub struct Slime {
    target: Option<Vec3>,
    pub collision_size: Vec2,
}

#[derive(Resource)]
pub struct SlimeSpeed {
    timer: Timer,
    step_up: f32,
    speed: f32,
}

#[derive(Resource)]
struct SlimeSpawner {
    timer: Timer,
}


#[derive(Resource)]
pub struct SlimeSheet {
    handle: Handle<TextureAtlas>,
    move_frames: [usize; 4],
}


pub struct SlimeDeath {
    pub entity: Entity
}

pub struct SlimeDamage {
    pub entity: Entity
}


impl Plugin for SlimePlugin {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system_to_stage(StartupStage::PreStartup, load_slime_sheet)
            .add_startup_system(spawn_spawner)
            .add_startup_system(setup_slime_updater)
            .add_event::<SlimeDeath>()
            .add_event::<SlimeDamage>()
            .add_system_set(
                SystemSet::on_enter(GameStates::Combat)
                    .with_system(inital_spawns)
                    .with_system(set_slime_speed)
            ).add_system_set(
                SystemSet::on_exit(GameStates::Combat)
                    .with_system(remove_slimes)
            ).add_system_set(
                SystemSet::on_update(GameStates::Combat)
                    .with_system(slime_movement)
                    .with_system(slime_spawn_handler)
                    .with_system(update_slimes)
                    .with_system(handle_slime_death.label("slime_death_logic"))
                    .with_system(handle_slime_damage)
            );
    }
}


fn load_slime_sheet(mut commands: Commands, assets: Res<AssetServer>, mut texture_atlases: ResMut<Assets<TextureAtlas>>) {
    let image = assets.load("slimes/slime_sprite_sheet.png");
    let atlas = TextureAtlas::from_grid(
        image,
        Vec2::splat(SLIME_TILE_SIZE),
        8,
        2,
        None,
        None,
    );

    let atlas_handle = texture_atlases.add(atlas);

    commands.insert_resource(SlimeSheet {
        handle: atlas_handle,
        move_frames: [0, 1, 2, 3],
    });
}

fn setup_slime_updater(mut commands: Commands) {
    commands
    .insert_resource( SlimeSpeed { timer: Timer::from_seconds(10.0, TimerMode::Repeating), speed: 0.5, step_up: 0.1 });
}


fn spawn_spawner(mut commands: Commands) {
    commands.insert_resource(
        SlimeSpawner { timer: Timer::from_seconds(1.0, TimerMode::Repeating) }
    );
}

fn inital_spawns(commands: Commands, slime_sheet: Res<SlimeSheet>) {
    spawn_slime(commands, slime_sheet, Vec2::new(0.5, 0.5), true);
}

fn set_slime_speed(
    mut slime_speed: ResMut<SlimeSpeed>
) {
    slime_speed.speed = 0.5;
}

fn spawn_slime(mut commands: Commands, slime_sheet: Res<SlimeSheet>, translation: Vec2, visible: bool) {
    let mut sprite = TextureAtlasSprite::new(slime_sheet.move_frames[0]);

    sprite.custom_size = Some(Vec2::splat(SLIME_TILE_SCALE));

    let slime = commands.spawn((
        SpriteSheetBundle {
            sprite: sprite,
            texture_atlas: slime_sheet.handle.clone(),
            transform: Transform {
                translation: Vec3::new(translation.x, translation.y, SPRITEZ),
                ..Default::default()
            },
            visibility: Visibility { is_visible: visible},
            ..Default::default()
        },
        FrameAnimation {
            timer: Timer::from_seconds(0.2, TimerMode::Repeating),
            frames: slime_sheet.move_frames.to_vec(),
            current_frame: 0,
            flip_x: false,
            frame_changed: true,
        },
        Health { health: 2, max_health: 2 },
        Slime { target: None, collision_size: Vec2::splat(0.08) },
        Name::new("Slime"),
    )).id();

    add_attack_area(
        commands,
        slime,
        1,
        Vec2::splat(0.08),
        Vec2::splat(0.0),
        0.7
    )
}


fn slime_spawn_handler(
    commands: Commands,
    camera_query: Query<&Transform, With<Camera>>,
    slime_sheet: Res<SlimeSheet>,
    time: Res<Time>,
    mut slime_spawner: ResMut<SlimeSpawner>
) {
    slime_spawner.timer.tick(time.delta());
    if slime_spawner.timer.just_finished() {
        
        let camera_transform = camera_query.single();
        let left_edge   = -1.0 * RESOLUTION - SLIME_TILE_SCALE + camera_transform.translation.x;
        let right_edge  = 1.0 * RESOLUTION + SLIME_TILE_SCALE + camera_transform.translation.x;
        let bottom_edge = -1.0 - SLIME_TILE_SCALE + camera_transform.translation.y;
        let top_edge    = 1.0 + SLIME_TILE_SCALE + camera_transform.translation.y;

        // TODO: make random a seeded resource?
        let mut rng = thread_rng();
        let (rand_x, rand_y) = match (rng.gen_bool(0.5), rng.gen_bool(0.5)) {
            (true, true) =>  (rng.gen_range(left_edge..right_edge), top_edge),
            (true, false) =>  (rng.gen_range(left_edge..right_edge), bottom_edge),
            (false, true) =>  (left_edge, rng.gen_range(bottom_edge..top_edge)),
            (false, false) =>  (right_edge, rng.gen_range(bottom_edge..top_edge)),
        };
        
        spawn_slime(commands, slime_sheet, Vec2::new(rand_x, rand_y), true)

    }
}

// TODO: this looks like it is breaking the no
fn slime_movement(
    player_query: Query<&GlobalTransform, With<Player>>,
    mut slime_query: Query<(&mut Transform, &mut Slime)>,
    slime_speed: Res<SlimeSpeed>,
    time: Res<Time>
) {

    let player_transform = player_query.single();
    let target_translation =  player_transform.translation().truncate();

    for (slime_transform, mut slime) in slime_query.iter_mut() {
        let increment = slime_speed.speed * SLIME_TILE_SCALE * time.delta_seconds();
        let slime_point = slime_transform.translation.truncate();
        let move_direction = (target_translation - slime_point).clamp_length_max(increment);

        slime.target = Some(Vec3::new(
            slime_transform.translation.x + move_direction.x, 
            slime_transform.translation.y + move_direction.y,
            SPRITEZ
        ));
    }

    let mut combinations = slime_query.iter_combinations_mut();

    while let Some([(_, mut slime_one), (slime_transform_two, slime_two)]) = combinations.fetch_next() {
        if let Some(tar) = slime_one.target {
            let collided_with_slime = collide(
                    tar,
                    slime_one.collision_size,
                    slime_transform_two.translation,
                    slime_two.collision_size,
                ).is_some();

            if collided_with_slime {
                slime_one.target = None;
            }
        }        
    }

    for (mut slime_transform, slime) in slime_query.iter_mut() {
        if let Some(make_move) = slime.target {
            slime_transform.translation =  make_move;
        }        
    }


}


fn remove_slimes(
    mut commands: Commands,
    slime_query: Query<Entity, With<Slime>>,
) {
    for slime_entity in slime_query.iter() {
        commands.entity(slime_entity).despawn_recursive()
    };    
}

fn update_slimes(
    mut slime_updater: ResMut<SlimeSpeed>,
    time: Res<Time>,
) {
    slime_updater.timer.tick(time.delta());

    if slime_updater.timer.just_finished() {
        slime_updater.speed += slime_updater.step_up;
    }    
}


fn handle_slime_death(
    mut commands: Commands,
    mut slime_death_events: EventReader<SlimeDeath>,
) {
    for slime_death_event in slime_death_events.iter() {
        commands.entity(slime_death_event.entity).despawn_recursive();        
    }

    slime_death_events.clear();
}


fn handle_slime_damage(
    mut slime_damage_events: EventReader<SlimeDamage>,
    mut slimes: Query<&mut TextureAtlasSprite, With<Slime>>,
) {
    for hurt_slime in slime_damage_events.iter() {
        if let Ok(mut sprite) = slimes.get_mut(hurt_slime.entity) {
            sprite.color = Color::ORANGE_RED;
        };
    }

    slime_damage_events.clear();
}