use bevy::{prelude::*};

use crate::GameStates;

pub struct TitlePlugin;

#[derive(Component)]
struct Title;

#[derive(Component)]
struct GameOver;

impl Plugin for TitlePlugin {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system(spawn_title_text.label("spawn_title"))
            .add_system_set(
                SystemSet::on_enter(GameStates::MainMenu)
                    .with_system(show_title)
            ).add_system_set(
                SystemSet::on_exit(GameStates::MainMenu)
                    .with_system(hide_title)
            ).add_system_set(
                SystemSet::on_update(GameStates::MainMenu)
                    .with_system(start_combat)
            ).add_system_set(
                SystemSet::on_enter(GameStates::GameOver)
                    .with_system(show_game_over)
            ).add_system_set(
                SystemSet::on_exit(GameStates::GameOver)
                    .with_system(hide_game_over)
            )
        ;
    }
}

fn spawn_title_text(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn((
        TextBundle::from_section(
            // Accepts a `String` or any type that converts into a `String`, such as `&str`
            "alasa!",
            TextStyle {
                font: asset_server.load("fonts/mago1.ttf"),
                font_size: 100.0,
                color: Color::WHITE,
            },
        ) 
        // Set the alignment of the Text
        .with_text_alignment(TextAlignment::CENTER)
        // Set the style of the TextBundle itself.
        .with_style(
            Style {
                align_self: AlignSelf::Center,
                position: UiRect {
                    left: Val::Px(40.0),
                    ..default()
                },
                ..default()
            }
        ),
        Title,
        Name::new("Title"),
    ));

    commands.spawn((
        TextBundle::from_section(
            // Accepts a `String` or any type that converts into a `String`, such as `&str`
            "press space to begin",
            TextStyle {
                font: asset_server.load("fonts/mago1.ttf"),
                font_size: 45.0,
                color: Color::WHITE,
            },
        ) 
        // Set the alignment of the Text
        .with_text_alignment(TextAlignment::TOP_CENTER)
        // Set the style of the TextBundle itself.
        .with_style(
            Style {
                align_self: AlignSelf::Center,
                position: UiRect {
                    left: Val::Px(50.0),
                    bottom: Val::Px(13.0),
                    ..default()
                },
                ..default()
            }
        ),
        Title,
        Name::new("Title (sub)"),
    ));

    commands.spawn((
        TextBundle::from_section(
            // Accepts a `String` or any type that converts into a `String`, such as `&str`
            "Game Over!",
            TextStyle {
                font: asset_server.load("fonts/mago1.ttf"),
                font_size: 100.0,
                color: Color::ORANGE_RED,
            },
        ) 
        // Set the alignment of the Text
        .with_text_alignment(TextAlignment::CENTER)
        // Set the style of the TextBundle itself.
        .with_style(
            Style {
                align_self: AlignSelf::Center,
                position: UiRect {
                    left: Val::Px(70.0),
                    ..default()
                },
                ..default()
            }
        ),
        Name::new("Game Over"),
        GameOver,
    ))
    // TODO: not sure how to set Visbility of text bundle on spawn
    .insert(
        Visibility { is_visible: false}
    );

    commands.spawn((
        TextBundle::from_section(
            // Accepts a `String` or any type that converts into a `String`, such as `&str`
            "hit esc",
            TextStyle {
                font: asset_server.load("fonts/mago1.ttf"),
                font_size: 45.0,
                color: Color::ORANGE_RED,
            },
        ) 
        // Set the alignment of the Text
        .with_text_alignment(TextAlignment::CENTER)
        // Set the style of the TextBundle itself.
        .with_style(
            Style {
                align_self: AlignSelf::Center,
                position: UiRect {
                    left: Val::Px(80.0),
                    bottom: Val::Px(13.0),
                    ..default()
                },
                ..default()
            }
        ),
        Name::new("Game Over (sub)"),
        GameOver,
    ))
    // TODO: not sure how to set Visbility of text bundle on spawn
    .insert(
        Visibility { is_visible: false}
    );
}


fn hide_game_over(
    mut title_query: Query<&mut Visibility, With<GameOver>>,
) {
    for mut title_visibility in title_query.iter_mut(){
        title_visibility.is_visible = false;
    }
}

fn show_game_over(
    mut title_query: Query<&mut Visibility, With<GameOver>>,
) {
    for mut title_visibility in title_query.iter_mut(){
        title_visibility.is_visible = true;
    }
}



fn hide_title(
    mut title_query: Query<&mut Visibility, With<Title>>,
) {
    for mut title_visibility in title_query.iter_mut(){
        title_visibility.is_visible = false;
    }
}

fn show_title(
    mut title_query: Query<&mut Visibility, With<Title>>,
) {
    for mut title_visibility in title_query.iter_mut(){
        title_visibility.is_visible = true;
    }
}

fn start_combat(keyboard: Res<Input<KeyCode>>, mut state: ResMut<State<GameStates>>) {
    if keyboard.just_pressed(KeyCode::Space) {
        println!("Changing to combat state");
        state.set(GameStates::Combat).unwrap();
    }
}