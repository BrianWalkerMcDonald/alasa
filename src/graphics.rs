use bevy::prelude::*;

pub struct GraphicsPlugin;


#[derive(Component)]
pub struct FrameAnimation {
    pub timer: Timer,
    pub frames: Vec<usize>,
    pub current_frame: usize,
    pub flip_x: bool,
    pub frame_changed: bool,
}

impl Plugin for GraphicsPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(Self::frame_animation);
    }
}

impl GraphicsPlugin {
    fn frame_animation(
        mut sprites_query: Query<(&mut TextureAtlasSprite, &mut FrameAnimation)>,
        time: Res<Time>,
    ) {
        for (mut sprite, mut animation) in sprites_query.iter_mut() {
            animation.timer.tick(time.delta());
            if animation.timer.just_finished() {
                animation.frame_changed = true;
                animation.current_frame = (animation.current_frame + 1) % animation.frames.len();
                sprite.index = animation.frames[animation.current_frame];
                sprite.flip_x = animation.flip_x;
            } else {
                animation.frame_changed = false;
            }
        }
    }
}