use bevy::{prelude::*, render::camera::ScalingMode, core_pipeline::clear_color::ClearColorConfig};

use crate::{RESOLUTION, GameStates};
use crate::player::Player;

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system(setup_camera)
            .add_system_set(
                SystemSet::on_update(GameStates::Combat)
                    .with_system(camera_follow.after("player_movement"))
            )
            ;
    }
}


fn setup_camera(mut commands: Commands) {
    // 2d camera
    let orthographic_settings = OrthographicProjection {
        top: 1.0,
        bottom: -1.0,
        right: 1.0 * RESOLUTION,
        left: -1.0 * RESOLUTION,

        scaling_mode: ScalingMode::None,
        // LEARN: I don't know how to look up what the default values are
        ..OrthographicProjection::default()
    };

    commands.spawn(
        Camera2dBundle {
            projection: orthographic_settings,
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::Custom(Color::rgb(36.0 / 255.0, 47.0 / 255.0, 7.0 / 255.0)),
            },
            ..Camera2dBundle::default()
        }
    );
}

fn camera_follow(
    player_query: Query<&Transform, With<Player>>,
    mut camera_query: Query<&mut Transform, (Without<Player>, With<Camera>)>,
) {
    // Note, this assume query will be single at all times, else the game will crash
    let player_transform = player_query.single();
    let mut camera_transform = camera_query.single_mut();

    camera_transform.translation.x = player_transform.translation.x;
    camera_transform.translation.y = player_transform.translation.y;
}

