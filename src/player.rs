use bevy::{prelude::*, sprite::MaterialMesh2dBundle, sprite::collide_aabb::collide};
use bevy_inspector_egui::Inspectable;

use crate::GameStates;
use crate::graphics::FrameAnimation;
use crate::enemy_attacks::{Health, add_attack_area, Attack};
use crate::slimes::{Slime, SlimeDeath, SlimeDamage};

pub const PLAYER_TILE_SIZE: f32 = 0.3;


pub struct PlayerPlugin;

#[derive(Default)]
pub struct PlayerDeath;


#[derive(Default)]
pub struct PlayerAttack;


#[derive(Default)]
pub struct PlayerHurt {
    pub damage: u32,
}

#[derive(Component)]
struct PlayerDamagedReaction {
    timer: Timer,
}

#[derive(Component, Inspectable)]
pub struct Player {
    speed: f32,
    pub collision_size: Vec2
}

impl Default for Player {
    fn default() -> Self {
        Player {
            speed: 0.8, 
            collision_size: Vec2::new(0.11, 0.13) 
        }
    }
}


#[derive(Component)]
struct PlayerState {
    state: PlayerStateTypes
}

impl Default for PlayerState {
    fn default() -> Self {
        PlayerState {
            state: PlayerStateTypes::Idle
        }
    }
}

#[derive(Component)]
struct HealthBar;

#[derive(Component)]
struct HealthBarBack;

#[derive(Resource)]
pub struct CharacterSheet {
    handle: Handle<TextureAtlas>,
    idle_frames: [usize; 4],
    run_frames: [usize; 6],
    die_frames: [usize; 6],
    atk_frames: [usize; 12],
    hit_frames: [usize; 2]
}


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum PlayerStateTypes {
    Idle,
    Attacking,
    MovingLeft,
    MovingRight,
}

#[derive(Resource)]
struct Score {
    killed: u64,
    alive: u64,
}

#[derive(Component)]
struct ScoreBoard;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system_to_stage(StartupStage::PreStartup, load_player_sheet)
            .add_startup_system(spawn_player)
            .add_startup_system(spawn_scoreboard.after("spawn_title"))
            .add_event::<PlayerDeath>()
            .add_event::<PlayerHurt>()
            .add_event::<PlayerAttack>()
            .add_system_set(
                SystemSet::on_enter(GameStates::Combat)
                    .with_system(setup_player)
                    .with_system(show_gui_elements)
                    .with_system(show_score_elements)
            ).add_system_set(
                SystemSet::on_exit(GameStates::Combat)
                    .with_system(hide_gui_elements)
                    .with_system(reset_player_sprite_color)
            ).add_system_set(
                SystemSet::on_update(GameStates::Combat)
                    .with_system(player_movement.label("player_movement"))
                    .with_system(update_player_animation)
                    .with_system(player_death_event)
                    .with_system(update_player_health_bar)
                    .with_system(player_attack_handler)
                    .with_system(player_damage_event.label("player_damage_logic"))
                    .with_system(update_scoreboard)
                    .with_system(update_alive_count)
            ).add_system_set(
                SystemSet::on_enter(GameStates::GameOver)
                    .with_system(set_player_animation_death)
            ).add_system_set(
                SystemSet::on_exit(GameStates::GameOver)
                    .with_system(hide_player)
                    .with_system(hide_score_elements)
            ).add_system_set(
                SystemSet::on_update(GameStates::GameOver)
                    .with_system(exit_to_menu)
            );
    }
}


fn spawn_scoreboard(mut commands: Commands, asset_server: Res<AssetServer>) {
    
    commands.insert_resource(Score { killed: 0, alive: 0 });

    commands.spawn((
        TextBundle::from_sections([
            TextSection::new(
                "0",
                TextStyle {
                    font: asset_server.load("fonts/mago1.ttf"),
                    font_size: 50.0,
                    color: Color::GOLD,
                },
            ),
            TextSection::new(
                " : ",
                TextStyle {
                    font: asset_server.load("fonts/mago1.ttf"),
                    font_size: 50.0,
                    color: Color::WHITE,
                },
            ),
            TextSection::new(
                "0",
                TextStyle {
                    font: asset_server.load("fonts/mago1.ttf"),
                    font_size: 50.0,
                    color: Color::GREEN,
                },
            ),
        ]) 
        // Set the alignment of the Text
        .with_text_alignment(TextAlignment::TOP_CENTER)
        // Set the style of the TextBundle itself.
        .with_style(Style {
            align_self: AlignSelf::Center,
            position: UiRect {
                bottom: Val::Px(350.0),
                right: Val::Px(50.0),
                ..default()
            },
            ..default()
        }),
        ScoreBoard,
        Name::new("ScoreBoard"),
    ))
    // TODO: not sure how to set Visbility of text bundle on spawn
    .insert(
        Visibility { is_visible: false}
    );
}


fn load_player_sheet(mut commands: Commands, assets: Res<AssetServer>, mut texture_atlases: ResMut<Assets<TextureAtlas>>) {
    let image = assets.load("player/adventurer_sheet.png");
    let atlas = TextureAtlas::from_grid(
        image,
        Vec2::new(50.0, 37.0),
        7,
        11,
        None,
        None
    );

    let atlas_handle = texture_atlases.add(atlas);

    commands.insert_resource(CharacterSheet {
        handle: atlas_handle,
        idle_frames: [0, 1, 2, 3],
        run_frames: [8, 9, 10, 11, 12, 13],
        die_frames: [7*9+3, 7*9+3, 7*9+3, 7*9+3, 7*9+4, 7*9+4],
        atk_frames: [7*5+6, 7*6, 7*6+1, 7*6+2, 7*6+3, 7*6+4, 7*6+5, 7*6+6, 7*7, 7*7+1, 7*7+2, 7*7+3],
        hit_frames: [3, 10],
    });
}


fn player_movement(
    mut player_query: Query<(&Player, &mut Transform, &mut PlayerState)>,
    keyboard: Res<Input<KeyCode>>,
    time: Res<Time>
) {
    // Note, this assume query will be single at all times, else the game will crash
    let (player, mut transform, mut player_state) = player_query.single_mut();


    let move_up = keyboard.pressed(KeyCode::W) || keyboard.pressed(KeyCode::Up);
    let move_down = keyboard.pressed(KeyCode::S) || keyboard.pressed(KeyCode::Down);
    let move_right = keyboard.pressed(KeyCode::D) || keyboard.pressed(KeyCode::Right);
    let move_left = keyboard.pressed(KeyCode::A) || keyboard.pressed(KeyCode::Left);

    let increment = player.speed * PLAYER_TILE_SIZE * time.delta_seconds();

    // todo: probably a better way to do this
    let state = match (move_up, move_down, move_left, move_right) {

        // move zero
        (true, true, true, true)     => { PlayerStateTypes::Attacking },
        (true, true, false, false)   => { PlayerStateTypes::Attacking },
        (false, false, true, true)   => { PlayerStateTypes::Attacking },
        (false, false, false, false) => { PlayerStateTypes::Attacking },

        // move left-ish
        (false, false, true, false) => { 
            transform.translation.x -= increment; 
            PlayerStateTypes::MovingLeft
        },
        (true, true, true, false)   => { 
            transform.translation.x -= increment; 
            PlayerStateTypes::MovingLeft
        },
        (true, false, true, false)  => { 
            transform.translation.x -= increment * 0.707; transform.translation.y += increment * 0.707; 
            PlayerStateTypes::MovingLeft
        },
        (false, true, true, false)  => { 
            transform.translation.x -= increment * 0.707; transform.translation.y -= increment * 0.707; 
            PlayerStateTypes::MovingLeft
        },

        // move right-ish
        (false, false, false, true) => { 
            transform.translation.x += increment; 
            PlayerStateTypes::MovingRight
        },
        (true, true, false, true)   => { 
            transform.translation.x += increment; 
            PlayerStateTypes::MovingRight
        },
        (true, false, false, true)  => { 
            transform.translation.x += increment * 0.707; transform.translation.y += increment * 0.707; 
            PlayerStateTypes::MovingRight
        },
        (false, true, false, true)  => { 
            transform.translation.x += increment * 0.707; transform.translation.y -= increment * 0.707; 
            PlayerStateTypes::MovingRight
        },

        // move veritcal
        (true, false, false, false) => { 
            transform.translation.y += increment; 
            if player_state.state == PlayerStateTypes::Attacking { PlayerStateTypes::MovingRight } else { player_state.state }
        },
        (true, false, true, true)   => { 
            transform.translation.y += increment; 
            if player_state.state == PlayerStateTypes::Attacking { PlayerStateTypes::MovingRight } else { player_state.state }
        },
        (false, true, false, false) => { 
            transform.translation.y -= increment; 
            if player_state.state == PlayerStateTypes::Attacking { PlayerStateTypes::MovingRight } else { player_state.state }
        },
        (false, true, true, true)   => { 
            transform.translation.y -= increment; 
            if player_state.state == PlayerStateTypes::Attacking { PlayerStateTypes::MovingRight } else { player_state.state }
        },
    };

    if state != player_state.state {
        player_state.state = state;
    }
}

fn spawn_player(
    mut commands: Commands, 
    player_sheet: Res<CharacterSheet>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let mut sprite = TextureAtlasSprite::new(player_sheet.run_frames[0]);

    sprite.custom_size = Some(Vec2::new(0.162_734_37 * 1.5, 0.120_423_436 * 1.5));

    let player = commands.spawn((
        SpriteSheetBundle {
            sprite: sprite,
            texture_atlas: player_sheet.handle.clone(),
            transform: Transform {
                translation: Vec3::new(0., 0., 800.0),
                ..Default::default()
            },
            visibility: Visibility { is_visible: false},
            ..Default::default()
        },
        FrameAnimation {
            timer: Timer::from_seconds(0.1, TimerMode::Repeating),
            frames: player_sheet.idle_frames.to_vec(),
            current_frame: 0,
            flip_x: false,
            frame_changed: true,
        },
        PlayerDamagedReaction { 
            timer: Timer::from_seconds(0.4, TimerMode::Once) 
        },
        Health { 
            health: 50, 
            max_health: 50 
        },
        Player::default() ,
        PlayerState::default(),
        Name::new("Player"),
    )).id();


    let health_bar = commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::new(Vec2::new(0.1, 0.02)))).into(),
            transform: Transform {
                translation: Vec3::new(0., -0.11, 0.0),
                ..Default::default()
            },
            material: materials.add(ColorMaterial::from(Color::ORANGE_RED)),
            ..default()
        },
        HealthBar,
        Name::new("Health Bar"),
    )).id();

    let health_bar_back = commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::new(Vec2::new(0.1, 0.02)))).into(),
            transform: Transform {
                translation: Vec3::new(0., -0.11, -1.0),
                ..Default::default()
            },
            material: materials.add(ColorMaterial::from(Color::BLACK)),
            ..default()
        },
        HealthBarBack,
        Name::new("Health Bar Back"),
    )).id();

    commands.entity(player).push_children(&[health_bar, health_bar_back]);

    add_attack_area(
        commands,
        player,
        1,
        Vec2::new(0.11, 0.15),
        Vec2::new(0.05, 0.0),
        1.0
    )
}

fn update_player_animation (
    mut sprites_query: Query<(&PlayerState, &mut FrameAnimation, &Children), Changed<PlayerState>>,
    mut player_attack_query: Query<&mut Transform, With<Attack>>,
    characters: Res<CharacterSheet>,
) {
    

    for (player_state, mut animation, children) in sprites_query.iter_mut() {
        println!("Updating player state: {:?}", player_state.state);
        animation.current_frame = 0;
        animation.frames = match player_state.state {
            PlayerStateTypes::Idle => characters.idle_frames.to_vec(),
            PlayerStateTypes::Attacking => characters.atk_frames.to_vec(),
            PlayerStateTypes::MovingRight => {
                animation.flip_x = false;
                characters.run_frames.to_vec()
            }
            PlayerStateTypes::MovingLeft => {
                
                animation.flip_x = true;
                characters.run_frames.to_vec()
            }
        };

        // LEARN: this seem inefficient? 
        for &child in children.iter() {
            if let Ok(mut player_attack_transform) = player_attack_query.get_mut(child) {
                player_attack_transform.translation.x = if animation.flip_x {
                    -0.05
                } else {
                    0.05
                };
            }
        }
    }    
}

fn player_attack_handler (
    player_info_query: Query<(&PlayerState, &FrameAnimation, &Children), (With<Player>, Changed<FrameAnimation>)>,
    player_attack_query: Query<(&GlobalTransform, &Attack)>,
    mut slimes: Query<(&mut Health, &GlobalTransform, &Slime, Entity), Without<Player>>,
    character_sheet: Res<CharacterSheet>,
    mut score_count: ResMut<Score>,
    mut attack_events: EventWriter<PlayerAttack>,
    mut slime_death_events: EventWriter<SlimeDeath>, 
    mut slime_damage_events: EventWriter<SlimeDamage>,
) {
    for (player_state, frame_animation, children) in player_info_query.iter() {

    
        let player_is_attacking = player_state.state == PlayerStateTypes::Attacking;
        let on_attack_frame = character_sheet.hit_frames.contains(&frame_animation.current_frame);
        let and_frame_changed = frame_animation.frame_changed;

        if player_is_attacking && on_attack_frame && and_frame_changed{
            println!("In attack state");
            attack_events.send_default();
            
            for &child in children.iter() {
                if let Ok((player_attack_transform, attack)) = player_attack_query.get(child) {

                    println!("We have attack entry");

                    for (mut health, target_transform, slime, entity) in slimes.iter_mut() {

                        let collided = collide(
                            player_attack_transform.translation(),
                            attack.area,
                            target_transform.translation(),
                            slime.collision_size,
                        ).is_some();

                        if collided {
                            health.health = health.health.saturating_sub(attack.damage);
                
                            if health.health == 0 {
                                slime_death_events.send(SlimeDeath { entity });
                                score_count.killed += 1;
                            } else {
                                slime_damage_events.send(SlimeDamage { entity });
                            }
                        }
                    }
                }
            }

        }
    }
}

fn update_alive_count(
    slimes: Query<&Slime>,
    mut score_count: ResMut<Score>,
){
    score_count.alive = slimes.iter().len() as u64;
}

fn update_scoreboard(
    score_count: Res<Score>,
    mut score_boards: Query<&mut Text, With<ScoreBoard>>,
) {
    if score_count.is_changed() {
        for mut scoreboard_text in score_boards.iter_mut() {
            scoreboard_text.sections[0].value = score_count.killed.to_string();
            scoreboard_text.sections[2].value = score_count.alive.to_string();
        }
    }
}


fn update_player_health_bar (
    health_query: Query<&Health, (With<Player>, Changed<Health>)>,
    mut health_bars: Query<&mut Transform, (With<HealthBar>, Without<Player>)>,
) {
    let mut transform = health_bars.single_mut();
    for health_status in health_query.iter() {
        println!("Updating Health Bar {:?}", health_status);
        transform.scale.x = health_status.health  as f32 / health_status.max_health as f32;
    }
}

fn set_player_animation_death (
    mut animation_query: Query<&mut FrameAnimation, With<Player>>,
    characters: Res<CharacterSheet>,
) {
    let mut animation = animation_query.single_mut();
    animation.frames = characters.die_frames.to_vec(); 
}


fn player_death_event(
    death_events: EventReader<PlayerDeath>,
    mut state: ResMut<State<GameStates>>
) {
    if !death_events.is_empty() {
        death_events.clear();
        println!("Changing to game over");
        state.set(GameStates::GameOver).unwrap();
    }
}

fn player_damage_event(
    damage_events: EventReader<PlayerHurt>,
    mut q_player_damage_reaction: Query<(&mut PlayerDamagedReaction, &mut TextureAtlasSprite), With<Player>>,
    time: Res<Time>,
) {
    let (mut player_damage_reaction, mut sprite) = q_player_damage_reaction.single_mut();

    if !damage_events.is_empty() {
        damage_events.clear();
        player_damage_reaction.timer.reset();
        sprite.color = Color::RED;
    }

    player_damage_reaction.timer.tick(time.delta());

    if player_damage_reaction.timer.just_finished() {
        sprite.color = Color::WHITE;
    }
}


fn reset_player_sprite_color(
    mut q_player_sprite: Query<&mut TextureAtlasSprite, With<Player>>,
) {
    let mut sprite = q_player_sprite.single_mut();
    sprite.color = Color::WHITE;
}


fn exit_to_menu(
    keyboard: Res<Input<KeyCode>>, 
    mut state: ResMut<State<GameStates>>
) {
    if keyboard.just_pressed(KeyCode::Escape) {
        println!("Changing to main menu");
        state.set(GameStates::MainMenu).unwrap();
    }
}

fn hide_player(
    mut player_query: Query<&mut Visibility, With<Player>>,
) {
    let mut player_visibility = player_query.single_mut();
    player_visibility.is_visible = false;
}

fn setup_player(
    mut player_query: Query<(&mut Visibility, &mut Health, &mut FrameAnimation, &mut TextureAtlasSprite), With<Player>>,
    character_sheet: Res<CharacterSheet>,
    mut score_count: ResMut<Score>,
) {
    for (
        mut player_visibility, 
        mut health, 
        mut animation, 
        mut sprite
    ) in player_query.iter_mut() {

        sprite.color = Color::WHITE;
        health.health = health.max_health;
        animation.frames = character_sheet.atk_frames.to_vec();
        player_visibility.is_visible = true;
        score_count.killed = 0;
        score_count.alive = 0;

    }
}

fn hide_gui_elements(
    mut health_bar_query: Query<&mut Visibility, Or<(With<HealthBar>, With<HealthBarBack>)> >,
) {
    for mut visibility in health_bar_query.iter_mut() {
        visibility.is_visible = false;
    }
}

fn show_gui_elements(
    mut health_bar_query: Query<&mut Visibility, Or<(With<HealthBar>, With<HealthBarBack>)> >,
) {
    for mut visibility in health_bar_query.iter_mut() {
        visibility.is_visible = true;
    }
}

fn hide_score_elements(
    mut health_bar_query: Query<&mut Visibility, With<ScoreBoard>>,
) {
    for mut visibility in health_bar_query.iter_mut() {
        visibility.is_visible = false;
    }
}

fn show_score_elements(
    mut health_bar_query: Query<&mut Visibility, With<ScoreBoard>>,
) {
    for mut visibility in health_bar_query.iter_mut() {
        visibility.is_visible = true;
    }
}
