Bevy setup notes:
https://bevyengine.org/learn/book/getting-started/setup/

sprite ideas:
https://legnops.itch.io/red-hood-character
https://rvros.itch.io/animated-pixel-hero
https://rvros.itch.io/pixel-art-animated-slime
https://jesse-m.itch.io/skeleton-pack
https://cainos.itch.io/pixel-art-top-down-basic
https://nimblebeastscollective.itch.io/magosfonts

tutorials / references:
https://github.com/bevyengine/bevy/blob/latest/examples/2d/sprite_sheet.rs
https://dev.to/sbelzile/making-games-in-rust-deploying-a-bevy-app-to-the-web-1ahn
https://dev.to/sbelzile/series/16367
https://bevy-cheatbook.github.io/platforms/wasm.html
0.6 tutorial: https://youtube.com/playlist?list=PLT_D88-MTFOPPl75g4WshL1Gx2bnGTUkz

https://bevyengine.org/learn/book/migration-guides/0.7-0.8/
https://bevyengine.org/news/bevy-0-7/
https://github.com/bevyengine/bevy/blob/7989cb2650cb84b67eba7c0343b48e8e3b318ba1/examples/ecs/hierarchy.rs

rabbit holes avoided:
* what to name it
* what ide to use
* can I set up CI/CD
* what background/banner/theme should I have on itch.io
* 

itch
https://starwest.itch.io/itch-page-image-templates
https://www.lexaloffle.com/bbs/?tid=40036