# alasa

A game inspired by Vampire Survivors


## building for wasm

1. cargo build --target wasm32-unknown-unknown --release
1. wasm-bindgen --out-dir ./out/ --target web ./target/wasm32-unknown-unknown/release/alasa.wasm
1. npx serve .
1. open http://localhost:3000